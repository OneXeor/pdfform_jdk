--fill - parse and fill pdf from json.
  Example: --fill in="./in.pdf" out="./out.pdf" json="./template.json" read-only=true
  Note: null values are skipped, to reset a value to an empty/false set it explicitly.
  
  in -- input pdf file path.
  out -- output pdf file path.
  json -- input json file path
  
  read-only -- true or other any value. Sets field as read only.
  check-required -- true or other any value. Will print required fields for witch no value was found.
  report-not-mapped -- true or other any value. Will print field names for witch no value where found.
  
--info - print fields info hierarchically.
  detailed -- true or other any value. Detailed fields info.
  Default is printed: <name> (<is_required>, <is_read_only>)

--help - print help.