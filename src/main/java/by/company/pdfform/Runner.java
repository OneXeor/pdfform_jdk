package by.company.pdfform;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDComboBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDListBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDPushButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

import by.company.pdfform.parser.PDFFiller;
import by.company.pdfform.parser.PDFInfo;
import by.company.pdfform.parser.JSONParser;
import by.company.pdfform.parser.filler.ButtonFiller;
import by.company.pdfform.parser.filler.ChoiceFiller;
import by.company.pdfform.parser.filler.IFillField;
import by.company.pdfform.parser.filler.TextFieldFiller;
import by.company.pdfform.parser.info.IReportFieldInfo;
import by.company.pdfform.parser.info.ReportButtonInfo;
import by.company.pdfform.parser.info.ReportChoiceInfo;
import by.company.pdfform.parser.info.ReportTextFieldInfo;
import by.company.pdfform.utils.AppProperties;
import by.company.pdfform.utils.InfoWriter;
import by.company.pdfform.utils.ResourceManager;

import static by.company.pdfform.utils.ResourceManager.checkFileAccess;
import static by.company.pdfform.utils.ResourceManager.handleIOException;
import static by.company.pdfform.utils.ResourceManager.READABLE;
import static by.company.pdfform.utils.ResourceManager.WRITEABLE;

/**
 * 
 *
 */
public class Runner {

	final static Logger logger = LogManager.getLogger(App.class.getName());

	/**
	 * 
	 * @param command
	 */
	public void run(Command command) {
		try {
			switch (command.getCommand()) {
			case "--fill": {
				this.executeFill(command.getParameters());
			}
				break;
			case "--info": {
				this.executeInfo(command.getParameters());
			}
				break;
			case "--help": {
				this.printHelp();
			}
				break;
			default:
				this.printHelp();
				throw new IllegalArgumentException(AppProperties.getProperty("msg.err.command.not.found") + ": " + command.getCommand());
			}
		} catch (IllegalArgumentException e) {
			InfoWriter.write(e.getMessage());
		}
	}

	private void printHelp() {
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(App.class.getResourceAsStream("help.txt"), "UTF-8"));) {
			System.out.println(br.lines().collect(Collectors.joining(System.lineSeparator())));
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	private void executeFill(Map<String, String> parameters) {
		String inPdfPath = parameters.get("in");
		if (inPdfPath == null) {
			throw new IllegalArgumentException(AppProperties.getProperty("msg.err.in.pdf.not.present"));
		}

		String outPdfPath = parameters.get("out");
		if (outPdfPath == null) {
			throw new IllegalArgumentException(AppProperties.getProperty("msg.err.out.pdf.not.present"));
		}

		String jsonPath = parameters.get("json");
		if (jsonPath == null) {
			throw new IllegalArgumentException(AppProperties.getProperty("msg.err.in.json.not.present"));
		}

		if (!checkFileAccess(jsonPath, READABLE)) {
			return;
		}

		Map<String, Object> map = JSONParser.parse(jsonPath);
		if (map.size() > 0) {
			if (!checkFileAccess(inPdfPath, READABLE)) {
				return;
			}
	
			handleIOException(() -> {
				try (FileInputStream inPdf = new FileInputStream(inPdfPath)) {
					@SuppressWarnings({ "serial" })
					byte[] result = new PDFFiller(new HashMap<Class<?>, IFillField<?>>() {
						{
							put(PDTextField.class, new TextFieldFiller());

							ChoiceFiller choiceFiller = new ChoiceFiller();
							put(PDComboBox.class, choiceFiller);
							put(PDListBox.class, choiceFiller);

							ButtonFiller buttonFiller = new ButtonFiller();
							put(PDPushButton.class, buttonFiller);
							put(PDCheckBox.class, buttonFiller);
							put(PDRadioButton.class, buttonFiller);
						}
					}, Boolean.parseBoolean(parameters.get("read-only")),
							Boolean.parseBoolean(parameters.get("check-required")),
							Boolean.parseBoolean(parameters.get("report-not-mapped"))).fillTemplate(inPdf, map);

					if (result != null && checkFileAccess(outPdfPath, WRITEABLE)) {
						Files.write(new File(outPdfPath).toPath(), result);
					}
				}
			}, inPdfPath);
		} else {
			InfoWriter.write(AppProperties.getProperty("msg.info.empty.json"));
		}
	}

	private void executeInfo(Map<String, String> parameters) {
		final String inPdfPath = parameters.get("in");
		if (inPdfPath == null) {
			throw new IllegalArgumentException(AppProperties.getProperty("msg.err.in.pdf.not.present"));
		}

		@SuppressWarnings({ "serial", "rawtypes" })
		PDFInfo info = new PDFInfo(new HashMap<Class<?>, IReportFieldInfo>() {
			{
				put(PDTextField.class, new ReportTextFieldInfo());
				ReportChoiceInfo reportChoiceInfo = new ReportChoiceInfo();
				put(PDComboBox.class, reportChoiceInfo);
				put(PDListBox.class, reportChoiceInfo);
				ReportButtonInfo reportButtonInfo = new ReportButtonInfo();
				put(PDPushButton.class, reportButtonInfo);
				put(PDCheckBox.class, reportButtonInfo);
				put(PDRadioButton.class, reportButtonInfo);
			}
		});

		try {
			Boolean.parseBoolean(parameters.get("detailed"));
		} catch (NullPointerException e) {
			InfoWriter.write(e.getMessage());
		}

		if (checkFileAccess(inPdfPath, READABLE)) {
			ResourceManager.handleIOException(() -> {
				info.printFields(ResourceManager.add(new FileInputStream(inPdfPath)), System.out,
						Boolean.parseBoolean(parameters.get("detailed")));
			}, inPdfPath);
		}
	}

}
