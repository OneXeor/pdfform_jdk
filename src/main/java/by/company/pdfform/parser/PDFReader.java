package by.company.pdfform.parser;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import by.company.pdfform.utils.ResourceManager;

/**
 * 
 *
 */
public class PDFReader {

	public PDFReader () {
		
	}
	
	/**
	 * 
	 * @param pdfStream
	 * @return
	 */
	public byte[] read(InputStream pdfStream) {
		final List<Object> holder = new ArrayList<>();
		ResourceManager.handleIOException(() -> {
			try (BufferedInputStream input = ResourceManager.add(new BufferedInputStream(pdfStream));) {
				byte[] b = new byte[input.available()];
				input.read(b);
				holder.add(b);
			}
		});
		return (byte[]) holder.get(0);
	}
}
