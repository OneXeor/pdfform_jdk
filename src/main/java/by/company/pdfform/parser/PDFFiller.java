package by.company.pdfform.parser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDNonTerminalField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTerminalField;
import by.company.pdfform.parser.filler.IFillField;
import by.company.pdfform.utils.AppProperties;
import by.company.pdfform.utils.InfoWriter;
import by.company.pdfform.utils.ResourceManager;


// System.setProperty("org.apache.pdfbox.baseParser.pushBackSize", "2024768");

/**
 * 
 *
 */
public class PDFFiller {
	final static Logger logger = LogManager.getLogger(PDFFiller.class.getName());

	private boolean readOnly;
	private boolean checkRequired;
	private boolean reportNotMapped;
	private StringBuilder report = new StringBuilder("");

	private Map<Class<?>, IFillField<?>> handlers;

	/**
	 * 
	 * @param handlers
	 * @param readOnly
	 * @param checkRequired
	 * @param reportNotMapped
	 */
	public PDFFiller(Map<Class<?>, IFillField<?>> handlers, boolean readOnly, boolean checkRequired, boolean reportNotMapped) {
		this.handlers = handlers;
		this.readOnly = readOnly;
		this.checkRequired = checkRequired;
		this.reportNotMapped = reportNotMapped;
	}

	/**
	 * 
	 * @param pdfStream
	 * @param reportStream
	 */
	public byte[] fillTemplate(InputStream pdfStream, Map<String, Object> json) {
		final List<byte[]> holder = new ArrayList<>();
		ResourceManager.handleIOException(() -> {
			try (ByteArrayOutputStream resultStream = ResourceManager.add(new ByteArrayOutputStream())) {

				PDDocument doc = PDDocument.load(ResourceManager.add(new ByteArrayInputStream(new PDFReader().read(pdfStream))));
				PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
				PDAcroForm acroForm = docCatalog.getAcroForm();

				walkAndFill(acroForm.getFields(), Optional.ofNullable(json));

				if (reportNotMapped) {
					String reportResult = report.toString();
					if (!reportResult.isEmpty()) {
						InfoWriter.write(AppProperties.getProperty("msg.info.no.mapping") + ":\n" + report.toString());
					}		
				}

				if (doc.isEncrypted()) { // TODO: Should be otherwise handled
					InfoWriter.write(AppProperties.getProperty("msg.info.remove.security"));
					doc.setAllSecurityToBeRemoved(true);
				}

				doc.save(resultStream);
				doc.close();
				holder.add(resultStream.toByteArray());
			}
		});
		return holder.get(0);
	}

	@SuppressWarnings("unchecked")
	private void walkAndFill(List<PDField> fields, Optional<Map<String, Object>> valuesMap) {
		for (int i = 0; i < fields.size(); i++) {
			final PDField field = fields.get(i);
			final Object value = valuesMap.isPresent() ? valuesMap.get().get(field.getPartialName()) : null;
			if (checkRequired && field.isRequired() && value == null) {
				report.append(AppProperties.getProperty("msg.info.no.for.required")).append(": ")
						.append(field.getPartialName()).append("\n");
			}

			if (value != null || reportNotMapped) {
				if (field instanceof PDNonTerminalField) {
					walkAndFill(((PDNonTerminalField) field).getChildren(),
							Optional.ofNullable(value instanceof Map ? (Map<String, Object>) value : null));
				} else {
					if (value != null) {
						@SuppressWarnings("rawtypes")
						IFillField filler = handlers.get(field.getClass());
						ResourceManager.handleIOException(() -> {
							if (filler != null) {
								filler.fill((PDTerminalField) field, value);
							}
						});
					} else if (reportNotMapped){
						report.append(AppProperties.getProperty("msg.info.partial.name")).append(" = ")
								.append(field.getPartialName()).append(", ")
								.append(AppProperties.getProperty("msg.info.full.name")).append(" = ")
								.append(field.getFullyQualifiedName()).append("\n");
					}
					field.setReadOnly(readOnly);
				}
			}
		}
	}

}
