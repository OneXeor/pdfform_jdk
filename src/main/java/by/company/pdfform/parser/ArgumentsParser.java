package by.company.pdfform.parser;

import java.util.HashMap;

import by.company.pdfform.Command;
import by.company.pdfform.utils.AppProperties;
import by.company.pdfform.utils.InfoWriter;

/**
 * 
 *
 */
public class ArgumentsParser {

	public static Command parse(String[] args) {
		try {
			if (args == null || (args != null && args.length == 0)) {
				throw new IllegalArgumentException(AppProperties.getProperty("msg.err.no.args"));
			}
			
			Command command = null;
			for (String arg : args) {
				String[] argPart = arg.split("=");
				if (argPart.length == 2) {
					command.getParameters().put(argPart[0], argPart[1]);
				} else if (argPart.length > 2) {
					throw new IllegalArgumentException(AppProperties.getProperty("msg.err.wrong.arg") + ": " + arg);
				} else {
					if (command != null) {
						throw new IllegalArgumentException(AppProperties.getProperty("msg.err.command.single "));
					} 
					if (!arg.startsWith("--")) {
						
						
						throw new IllegalArgumentException(AppProperties.getProperty("msg.err.command.format") + ": " + arg);
					}
					command = new Command(arg, new HashMap<String, String>());
				}

			}
			return command;
		} catch (IllegalArgumentException e) {
			InfoWriter.write(e.getMessage());
			return new Command("--help", null);
		}
	}
}
