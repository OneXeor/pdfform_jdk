package by.company.pdfform.parser.info;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

/**
 * 
 *
 */
public class ReportTextFieldInfo implements IReportFieldInfo<PDTextField> {

	final static Logger logger = LogManager.getLogger(ReportTextFieldInfo.class.getName());

	@Override
	public void report(PDTextField field, StringBuilder report, String prefix) throws IOException {
		String defaultValue = field.getDefaultValue();
		if (!defaultValue.isEmpty()) {
			report.append(prefix).append("default: ").append(defaultValue).append("\n");
		}
		report.append(prefix).append("max length: ").append(field.getMaxLen()).append("\n")
				.append(prefix).append("is comb: ").append(field.isComb() ).append("\n")
				.append(prefix).append("is file select: ").append(field.isFileSelect() ).append("\n")
				.append(prefix).append("is multiline: ").append(field.isMultiline() ).append("\n")
				.append(prefix).append("is password: ").append(field.isPassword() ).append("\n");
	}
	
	

}
