package by.company.pdfform.parser.info;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.interactive.form.PDField;

/**
 * 
 *
 * @param <T>
 */
public interface IReportFieldInfo<T extends PDField> {

	/**
	 * 
	 * @param field
	 * @param prefix TODO
	 * @param value
	 * @throws IOException
	 */
	public abstract void report(T field, StringBuilder report, String prefix) throws IOException;
}
