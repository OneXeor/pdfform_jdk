package by.company.pdfform.parser.info;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.interactive.form.PDChoice;

/**
 * 
 *
 */
public class ReportChoiceInfo implements IReportFieldInfo<PDChoice> {

	final static Logger logger = LogManager.getLogger(ReportChoiceInfo.class.getName());

	@Override
	public void report(PDChoice field, StringBuilder report, String prefix) throws IOException {
		List<String> defaultValue = field.getDefaultValue();
		if (defaultValue.size() > 0 ) {
			report.append(prefix).append("default: ").append(defaultValue).append("\n");
		}
		report.append(prefix).append("multiselect: ").append(field.isMultiSelect()).append("\n")
				.append(prefix).append("options: ").append(field.getOptions()).append("\n");
	}

}
