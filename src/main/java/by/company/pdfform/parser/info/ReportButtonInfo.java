package by.company.pdfform.parser.info;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.interactive.form.PDButton;

/**
 * 
 *
 */
public class ReportButtonInfo implements IReportFieldInfo<PDButton> {

	final static Logger logger = LogManager.getLogger(ReportButtonInfo.class.getName());

	@Override
	public void report(PDButton field, StringBuilder report, String prefix) throws IOException {
		String defaultValue = field.getDefaultValue();
		if (!defaultValue.isEmpty()) {
			report.append(prefix).append("default: ").append(defaultValue).append("\n");
		}
		report.append(prefix).append("on values: ").append(field.getOnValues()).append("\n");
		/*if (field.getClass() == PDCheckBox.class) {
			PDCheckBox checkBox = (PDCheckBox) field;
			
			
		} else if (field.getClass() == PDPushButton.class) {
			PDPushButton pushButton = (PDPushButton) field;
			
		} else if (field.getClass() == PDRadioButton.class) {
			PDRadioButton radioButton = (PDRadioButton) field;
		}*/
		
	}

	

}
