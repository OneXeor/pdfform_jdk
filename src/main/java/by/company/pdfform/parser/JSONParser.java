package by.company.pdfform.parser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import by.company.pdfform.utils.ResourceManager;

/**
 * 
 *
 */
public class JSONParser {

	final static Logger logger = LogManager.getLogger(JSONParser.class.getName());

	/**
	 * 
	 * @param jsonStringForMap
	 * @return
	 */
	public static Map<String, Object> parse(String path) {
		final Map<String, Object> seanseMap = new HashMap<>();
		ResourceManager.handleIOException(() -> {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"));) {
				String jsonStringForMap = br.lines().collect(Collectors.joining(System.lineSeparator()));
				Moshi moshi = new Moshi.Builder().build();
				Type map = Types.newParameterizedType(Map.class, String.class, Object.class);
				JsonAdapter<Map<String, Object>> jsonAdapter = moshi.adapter(map);
				seanseMap.putAll(jsonAdapter.fromJson(jsonStringForMap));
			}
		}, path);
		return seanseMap;
	}
}
