package by.company.pdfform.parser.filler;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.interactive.form.PDButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDPushButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioButton;

/**
 * 
 *
 */
public class ButtonFiller implements IFillField<PDButton> {

	final static Logger logger = LogManager.getLogger(ButtonFiller.class.getName());
	
	private boolean offOnWrongValue;

	public ButtonFiller() {
		this(true);
	}
	
	public ButtonFiller(boolean offOnWrongValue) {
		this.offOnWrongValue = offOnWrongValue;
	}

	@Override
	public void fill(PDButton field, Object value) throws IOException {
		if (field.getClass() == PDCheckBox.class) {
			PDCheckBox checkBox = (PDCheckBox) field;
			boolean booleanValue = Boolean.parseBoolean(value.toString());
			logger.debug(checkBox.getOnValues());
			if (booleanValue || checkBox.getOnValue().equals(value.toString())) {
				checkBox.check();
			} else if (!booleanValue || value.toString().equals("Off") || offOnWrongValue) {
				checkBox.unCheck();
			} else {
				
			}

		} else if (field.getClass() == PDPushButton.class) {
			PDPushButton pushButton = (PDPushButton) field;
			pushButton.setValue(value.toString());

		} else if (field.getClass() == PDRadioButton.class) {
			PDRadioButton radioButton = (PDRadioButton) field;
			logger.debug(radioButton.getOnValues());
			if (radioButton.getOnValues().contains(value.toString())) {
				radioButton.setValue(value.toString());
			} else if (value.toString().equals("Off") || offOnWrongValue) {
				radioButton.setValue("Off");
			} else {

			}
		}
	}

}
