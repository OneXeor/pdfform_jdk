package by.company.pdfform.parser.filler;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.interactive.form.PDTerminalField;

/**
 * 
 *
 * @param <T>
 */
public interface IFillField<T extends PDTerminalField> {

	/**
	 * 
	 * @param field
	 * @param value
	 * @throws IOException
	 */
	public abstract void fill(T field, Object value) throws IOException;
}
