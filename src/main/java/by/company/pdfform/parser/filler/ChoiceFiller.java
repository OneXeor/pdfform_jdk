package by.company.pdfform.parser.filler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.interactive.form.PDChoice;

/**
 * 
 *
 */
public class ChoiceFiller implements IFillField<PDChoice> {

	final static Logger logger = LogManager.getLogger(ChoiceFiller.class.getName());
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void fill(PDChoice field, Object value) throws IOException {
		List list = null;
		if (value instanceof List) {
			if (((List) value).size() > 0 ) {
				if (field.isMultiSelect()) {
					list = (List) value;
				} else {
					list = new ArrayList<>();
					list.add(((List)value).get(0));
				}
			}
		} else if (value instanceof Integer) {
			list = new ArrayList<>();
			list.add(value);
		}

		if (list == null || (list != null && list.size() == 0)) {
			return;
		}

		if (field.isMultiSelect()) {
			field.setSelectedOptionsIndex(list);
		}
		
		List<String> options = field.getOptions();
		StringBuilder resultValue = new StringBuilder("");
		String optionValue = null;
		for (int i = 0; i < list.size(); i++) {
			try {
				int index = Double.valueOf(list.get(i).toString()).intValue();
				if ((optionValue = options.get(index)) != null) {
					resultValue.append(optionValue).append(", ");
				}		
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		if (resultValue.length() >= 2) {
			field.setValue(resultValue.substring(0, resultValue.length() - 2));
		}

	}

}
