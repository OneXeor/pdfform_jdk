package by.company.pdfform.parser.filler;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

/**
 * 
 *
 */
public class TextFieldFiller implements IFillField<PDTextField> {

	final static Logger logger = LogManager.getLogger(TextFieldFiller.class.getName());
	
	@Override
	public void fill(PDTextField field, Object value) throws IOException {
		field.setValue(value.toString());
	}

}
