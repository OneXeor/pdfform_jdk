package by.company.pdfform.parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDNonTerminalField;
import by.company.pdfform.parser.info.IReportFieldInfo;
import by.company.pdfform.utils.ResourceManager;

/**
 * 
 *
 */
public class PDFInfo {

	final static Logger logger = LogManager.getLogger(PDFInfo.class.getName());

	@SuppressWarnings("rawtypes")
	private Map<Class<?>, IReportFieldInfo> handlers = new HashMap<>();

	/**
	 * 
	 * @param handlers
	 */
	@SuppressWarnings("rawtypes")
	public PDFInfo(Map<Class<?>, IReportFieldInfo> handlers) {
		this.handlers = handlers;
	}

	/**
	 * 
	 * @param pdfStream
	 * @param reportStream
	 */
	public void printFields(InputStream pdfStream, OutputStream reportStream, boolean detailed) {
		ResourceManager.handleIOException(() -> {
			PDDocument doc = PDDocument.load(new ByteArrayInputStream(new PDFReader().read(pdfStream)));
			PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
			PDAcroForm acroForm = docCatalog.getAcroForm();
			StringBuilder report = new StringBuilder("");
			justDoIt(acroForm.getFields(), report, "", detailed);
			byte[] reportBytes = report.toString().getBytes("UTF-8");
			reportStream.write(reportBytes, 0, reportBytes.length);
		});
	}

	@SuppressWarnings("unchecked")
	private void justDoIt(List<PDField> fields, StringBuilder fieldsReport, String tab, boolean detailed)
			throws IOException {
		for (PDField field : fields) {
			fieldsReport.append(tab).append(field.getPartialName()).append(" ( ").append(field.isRequired())
					.append(", ").append(field.isReadOnly()).append(" )").append("\n");

			if (detailed) {
				@SuppressWarnings("rawtypes")
				IReportFieldInfo reportHandler = handlers.get(field.getClass());
				if (null != reportHandler) {
					reportHandler.report(field, fieldsReport, tab + "\t");
				}
			}

			if (field instanceof PDNonTerminalField) {
				justDoIt(((PDNonTerminalField) field).getChildren(), fieldsReport, tab + "\t", detailed);
			}
		}
	}

}
