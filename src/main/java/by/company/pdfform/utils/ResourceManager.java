package by.company.pdfform.utils;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UTFDataFormatException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;

import com.squareup.moshi.JsonDataException;
import com.squareup.moshi.JsonEncodingException;

import by.company.pdfform.parser.PDFFiller;

import static by.company.pdfform.utils.AppProperties.getProperty;

/**
 * 
 *
 */
public class ResourceManager {
	final static Logger logger = LogManager.getLogger(PDFFiller.class.getName());

	@FunctionalInterface
	public static interface ExcpetionAbleFunction {
		public void invoke() throws IOException;
	}

	private static List<Closeable> closeablestreams = new ArrayList<>();

	public static <T extends Closeable> T add(T closeable) {
		closeablestreams.add(closeable);
		return closeable;
	}

	public static void releaseResources() {
		for (Closeable closeable : closeablestreams) {
			try {
				if (closeable != null) {
					closeable.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}

	public static boolean handleIOException(ExcpetionAbleFunction func) {
		return handleIOException(func, "");
	}

	public static boolean handleIOException(ExcpetionAbleFunction func, String path) {
		boolean exception = false;
		try {
			try {
				func.invoke();
			} catch (IOException e) {
				exception = true;
				throw e;
			}
		} catch (InvalidPasswordException e) {
			InfoWriter.write(getProperty("msg.err.invalid.pass") + ": " + e.getMessage());
			logger.error(e.getMessage());
		} catch (JsonEncodingException | JsonDataException e) {
			InfoWriter.write(getProperty("msg.err.json.parse") + ": " + e.getMessage() + " " + getProperty("msg.file")
					+ ": " + path);
			logger.error(e.getMessage());
		} catch (EOFException e) {
			InfoWriter.write(getProperty("msg.err.eof.file") + ": " + path);
			logger.error(e.getMessage());
		} catch (InterruptedIOException e) {
			InfoWriter.write(getProperty("msg.err.io.interrupted") + ": " + path);
			logger.error(e.getMessage());
		} catch (UTFDataFormatException e) {
			InfoWriter.write(getProperty("msg.err.reading.utf8") + ": " + e.getMessage() + getProperty("msg.file")
					+ ": " + path);
			logger.error(e.getMessage());
		} catch (IOException e) {
			InfoWriter.write(getProperty("msg.err.io") + ": " + e.getMessage());
			logger.error(e.getMessage());
		}

		return exception;
	}

	public final static int READABLE = 1;
	public final static int WRITEABLE = 2;

	/**
	 * 
	 * @param path
	 * @param permissions
	 * @return
	 */
	public static boolean checkFileAccess(String path, int permissions) {
		boolean result = false;
		try {
			Path pathObj = Paths.get(path);
			if ((READABLE & permissions) == READABLE && !Files.exists(pathObj)) {
				String errMsg = getProperty("msg.err.file.not.found") + ": " + path;
				InfoWriter.write(errMsg);
				logger.error(errMsg);
				return result;
			}
			switch (permissions) {
			case 1: {
				result = Files.isReadable(pathObj);
			}
				break;
			case 2: { 
				result = Files.isWritable(pathObj.getParent());
			}
				break;
			case 3: {
				result = Files.isReadable(pathObj) && Files.isWritable(pathObj);
			}
				break;
			default:
				break;
			}

			if (!result) {
				InfoWriter.write(getProperty("msg.err.access.denied") + ":" + path);
			}
		} catch (InvalidPathException e) {
			InfoWriter.write(getProperty("msg.err.invalid.path") + ", " + e.getReason() + " "
					+ getProperty("msg.at.pos") + " " + e.getIndex() + ": " + path);
			logger.error(e.getMessage());
		}

		return result;
	}

}
