package by.company.pdfform.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.company.pdfform.App;

/**
 * 
 *
 */
public class AppProperties {
	final static Logger logger = LogManager.getLogger(AppProperties.class.getName());
	private static Properties properties;

	public static void load(){
		try (InputStream fis = App.class.getResourceAsStream("messages.properties");) {
			properties = new Properties();
			properties.load(fis);
		} catch (IOException e) {
			logger.error(e.getMessage());
			System.exit(-1);
		}
	}

	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	public static String getProperty(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue);
	}
}
