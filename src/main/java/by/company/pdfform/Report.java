package by.company.pdfform;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
// import java.io.exce

public class Report {

	private ByteArrayOutputStream stream;

	public Report(boolean report) {
		if (report) {
			this.stream = new ByteArrayOutputStream();
		}
	}

	public void write(String reportPart) {
		if (this.stream == null) {
			return;
		}
		try {
			this.stream.write(reportPart.getBytes("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			this.closeStream();
		}
	}

	/**
	 * 
	 * @return
	 */
	public void save(String path) {
		if (this.stream == null) {
			return;
		}
		new File(path).toPath();
		try {
			// this.stream.flush();
			Files.write(new File(path).toPath(), this.stream.toByteArray());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			this.closeStream();
		}
	}

	private void closeStream() {
		if (this.stream != null) {
			try {
				this.stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				this.stream = null;
			}
		}
	}
}
