package by.company.pdfform;

import java.util.Map;

/**
 * 
 *
 */
public class Command {

	private String command;
	
	private Map<String, String> parameters;

	/**
	 * 
	 * @param command
	 * @param parameters
	 */
	public Command(String command, Map<String, String> parameters) {
		this.command = command;
		this.parameters = parameters;
	}

	/**
	 * 
	 * @return
	 */
	public String getCommand() {
		return command;
	}

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getParameters() {
		return parameters;
	}	
}
