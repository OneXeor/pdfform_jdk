package by.company.pdfform;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.company.pdfform.parser.ArgumentsParser;
import by.company.pdfform.utils.AppProperties;
import by.company.pdfform.utils.ResourceManager;

/**
 *
 *
 */
public class App {
	
	final static Logger logger = LogManager.getLogger(App.class.getName());
	
	public static void main(String[] args) {
		try {
		  AppProperties.load();
		  new Runner().run(ArgumentsParser.parse(args));
		  ResourceManager.releaseResources();
		} catch(Throwable e) {
			logger.error(e.getMessage());
		}
	}
	
	
}
