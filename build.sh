#!/bin/sh
mkdir -p ./out
javac -cp "lib/*" -sourcepath ./src/main/java -d ./out/ -encoding UTF8 @versionargs ./src/main/java/by/company/pdfform/App.java
cp -r ./src/main/resources/* ./out
jar cvfm ./pdfform.jar ./src/main/resources/META-INF/MANIFEST.MF -C ./out/ .
rm -r ./out
